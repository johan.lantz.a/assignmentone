let loan = 0;

let salary = 0;
let bankFunds = 0;
let payLoanBtn = document.getElementById("payLoanBtn");
payLoanBtn.style.visibility = "hidden"

let price = 0;

let outstandingLoan = document.getElementById("outstandingLoan");
outstandingLoan.style.visibility = "hidden"

const computersElement = document.getElementById("computers");
const titleElement = document.getElementById("title");
const priceElement = document.getElementById("price");
const featureElement = document.getElementById("feature");
const specsElement = document.getElementById("specs");
//const idElement = document.getElementById("id");
const imgElement = document.getElementById("img");


// every time you press the work button 100 adds to salary count
function work() {
  salary += 100;
  document.getElementById("salary").innerText = salary + " kr";
}

// when pressing pay salary and loan is 0 or NaN, the salary amount is added to bankFunds and salary is set to 0.
// If there is a loan, 10% of the salary is put towards the loan and 90% is added to bankFunds, unless 10% of the 
// salary is more than the outstanding loan amount in which case the required amount to pay of the loan is 
//deducted from the salary before adding salary to bank funds.
function paySalary() {
  if (loan <= 0 || Number.isNaN(loan)) {
    bankFunds += salary;
    salary = 0;
    document.getElementById("bankFunds").innerText = bankFunds + " kr";
    document.getElementById("salary").innerText = salary + " kr";
  } else {
    if (loan - salary * 0.1 < 0) {
      salary -= loan;
      loan = 0
      bankFunds += salary;
      salary = 0;
      document.getElementById("bankFunds").innerText = bankFunds + " kr";
      document.getElementById("salary").innerText = salary + " kr";
      document.getElementById("loan").innerText = loan + " kr";
        outstandingLoan.style.visibility = "hidden"
        payLoanBtn.style.visibility = "hidden"
    } else {
      bankFunds += salary * 0.9;
      loan = loan - salary * 0.1;
      salary = 0;
      document.getElementById("bankFunds").innerText = bankFunds + " kr";
      document.getElementById("salary").innerText = salary + " kr";
      document.getElementById("loan").innerText = loan + " kr";
    }
  }
}
// Take loan checks if loan is nan or if there is a loan (loan > 0 mean you have a loan). If there is no current loan active an input i taken and checked against 2 times the bankFunds
// to check if the amount is allowed to borrow.
function takeLoan() {
  if (loan <= 0 || Number.isNaN(loan)) {
    loan = prompt("current bank funds: " + bankFunds);
    loan = parseInt(loan);
    if (bankFunds * 2 >= loan) {
      bankFunds += loan;
      document.getElementById("bankFunds").innerText = bankFunds + " kr";
      document.getElementById("loan").innerText = loan + " kr";
      payLoanBtn.style.visibility = "visible"
      outstandingLoan.style.visibility = "visible"
    } else {
      alert("cant loan that amount");
      loan = 0;
    }
  } else {
    alert("cant have multiple loans");
  }
}

// payLoan checks if loan is more than 0. If there is a loan, the button opens a prompt where input is checked against bankFunds to check if you have the available balance to pay
// towards the loan. If the loan is fully paid the function hides the payLoan button and outstanding balance.
function payLoan() {
  if (loan > 0) {
    payment = prompt("Amount to repay " + loan + " kr");
    payment = parseInt(payment);
    if (payment > loan) {
      payment = loan;
    }
    if (bankFunds - payment >= 0) {
      bankFunds -= payment;
      loan -= payment;
      document.getElementById("bankFunds").innerText = bankFunds + " kr";
      document.getElementById("loan").innerText = loan + " kr";
      if (loan == 0) {
        payLoanBtn.style.visibility = "hidden"
        outstandingLoan.style.visibility = "hidden"
      }
    } else {
      alert("insufficient funds");
    }
  } 
}

// computersElement.appendChild(document.createTextNode(computer.features))

let computers = [];

//fetch API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then(response => response.json())
  .then(data => computers = data)
  .then(computers => addComputersToStore(computers));
  


  const addComputersToStore = (computers) => {
    computers.forEach(x => addComputerToMenu(x));
    priceElement.innerText = computers[0].price;
    featureElement.innerText = computers[0].description;
    specsElement.innerText = computers[0].specs;
   // idElement.innerText = computers[0].id;
    titleElement.innerText = computers[0].title;
    imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image;
  }

 
  const addComputerToMenu = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));   
    computersElement.appendChild(computerElement);

}

  const handleComputerMenuChanges = e => {
    const selectedComputer = computers[e.target.selectedIndex]
    priceElement.innerText = selectedComputer.price;
    featureElement.innerText = selectedComputer.description;
    specsElement.innerText = selectedComputer.specs;
    titleElement.innerText = selectedComputer.title;
   // idElement.innerText = selectedComputer.id;
    imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
   
 
  }

  function buyComputer(){
    const selectedComputer = computers[computersElement.selectedIndex]
    price = selectedComputer.price
    if (price <= bankFunds) {
      bankFunds -= price
      document.getElementById("bankFunds").innerText = bankFunds + " kr";
      alert("You have purchased this computer");
    } else{
      alert("insufficient funds!")
    }
  }
  
  computersElement.addEventListener("click", handleComputerMenuChanges)

  
